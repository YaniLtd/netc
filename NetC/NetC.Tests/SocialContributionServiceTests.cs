﻿using NetC.Services;
using Xunit;

namespace NetC.Tests
{
    public class SocialContributionServiceTests
    {
        [Fact]
        public void GetSocialContributionAmount_ReturnsZeroForSalaryOf1000AndBelow()
        {
            //Act
            var actualResult = new SocialContributionService().GetSocialContributionAmount(1000, 15);

            //Assert
            Assert.Equal(0, actualResult);
        }
        [Fact]
        public void GetSocialContributionAmount_ReturnsCorrectForSalaryAbove3000()
        {
            //Act
            var actualResult = new SocialContributionService().GetSocialContributionAmount(4000, 15);

            //Assert
            Assert.Equal(300, actualResult);
        }
        [Theory]
        [InlineData(1600, 15, 90)]
        [InlineData(1600, -15, -90)]
        [InlineData(1979, 10, 97.9)]
        public void GetSocialContributionAmount_ReturnsCorrectAnswers(double salary, double socialContributionPercent, double expectedResult)
        {
            //Act
            var actualResult = new SocialContributionService().GetSocialContributionAmount(salary, socialContributionPercent);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}