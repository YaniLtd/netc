﻿using NetC.Services;
using Xunit;

namespace NetC.Tests
{
    public class IncomeTaxServiceTests
    {
        [Fact]
        public void GetIncomeTaxAmount_ReturnsZeroForSalaryOf1000AndBelow() 
        {
            //Act
            var actualResult = new IncomeTaxService().GetIncomeTaxAmount(1000, 10);

            //Assert
            Assert.Equal(0, actualResult);
        }
        [Theory]
        [InlineData(5540, 10, 454)]
        [InlineData(10000, 2.5, 225)]
        [InlineData(1979, -10, -97.9)]
        public void GetIncomeTaxAmount_ReturnsCorrectAmount(double salary, double taxPercent, double expectedResult) 
        {
            //Act
            var actualResult = new IncomeTaxService().GetIncomeTaxAmount(salary, taxPercent);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}