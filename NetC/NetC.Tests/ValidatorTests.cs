﻿using NetC.Utils;
using Xunit;

namespace NetC.Tests
{
    public class ValidatorTests
    {
        [Fact]
        public void ValidateSalaryInput_ReturnsFalseForNegativeSalary() 
        {
            //Act
            var result = new Validator().ValidateSalaryInput("-2500");

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidateSalaryInput_ReturnsFalseIfTextIsPresent()
        {
            //Act
            var result = new Validator().ValidateSalaryInput("2500 IDR");

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidateSalaryInput_ReturnsTrue()
        {
            //Act
            var result = new Validator().ValidateSalaryInput("2500");

            //Assert
            Assert.True(result);
        }
    }
}