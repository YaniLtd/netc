﻿namespace NetC.CLI.Contracts
{
    public interface INetCSpeaker
    {
        /// <returns>"Enter the gross value of a salary in IDR and see the net amount (use dot for floating point): "</returns>
        string AskForGrossSalary();
        /// <returns>"Please enter a non negative number only (use dot for floating point): "</returns>
        string AskForGrossSalaryAfterBadInput();
        /// <returns>"Do you want to see the net value of another salary? (yes/no)"</returns>
        string AskIfAnotherGrossSalaryShouldBeRead();
        /// <returns>"Do you want to see the income tax and the social contribution of the entered gross salary? (yes/no)"</returns>
        string AskToShowIncomeTaxAndSocialContribution();
        /// <returns>The NetC logo in a fancy format</returns>
        string DisplayLogo();
        /// <returns>"Good bye :)"</returns>
        string SayGoodBye();
        /// <param name="incomeTax">The income tax in money value</param>
        /// <returns>$"The income tax paid is {incomeTax} IDR"</returns>
        string ShowIncomeTax(double incomeTax);
        /// <param name="netSalary">The net salary in money value</param>
        /// <returns>$"Net salary is {netSalary} IDR"</returns>
        string ShowNetSalary(double netSalary);
        /// <param name="socialContribution">The social contribution in money value</param>
        /// <returns>$"The social contribution is {socialContribution} IDR"</returns>
        string ShowSocialContribution(double socialContribution);
    }
}