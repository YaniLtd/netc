﻿using NetC.CLI.Contracts;
using NetC.Services.Contracts;
using NetC.Utils.Contracts;
using System;

namespace NetC.CLI
{
    public class Engine : IEngine
    {
        private const double _incomeTaxPercent = 10;
        private const double _socialContributionPercent = 15;
        private readonly IReader _reader;
        private readonly IWriter _writer;
        private readonly INetCSpeaker _netCSpeaker;
        private readonly IValidator _validator;
        private readonly IIncomeTaxService _incomeTaxService;
        private readonly ISocialContributionService _socialContributionService;
        private readonly INetSalaryService _netSalaryService;
        public Engine(IReader reader, IWriter writer, INetCSpeaker netCSpeaker, IValidator validator,
            IIncomeTaxService incomeTaxService, ISocialContributionService socialContributionService, INetSalaryService netSalaryService)
        {
            _reader = reader;
            _writer = writer;
            _netCSpeaker = netCSpeaker;
            _validator = validator;
            _incomeTaxService = incomeTaxService;
            _socialContributionService = socialContributionService;
            _netSalaryService = netSalaryService;
        }
        /// <summary>
        /// First we show the NetC logo.
        /// Then we declare all variables that we will be using.
        /// In a do-while loop, we ask the person to provide a gross salary.
        /// After we calculate the net salary we display it and ask the user if he/she wants to see the amount gone for income tax and social contribution.
        /// Regardless of the answer, we ask the user if he/she wants to see the net salary for another gross value.
        /// If not the program ends.
        /// </summary>
        public void Run()
        {
            Console.SetWindowSize(200, 43);
            Console.ForegroundColor = ConsoleColor.Green;
            _writer.WriteLine(_netCSpeaker.DisplayLogo());
            _writer.WriteLine();
            string answer = string.Empty;
            string salaryInput;
            double salary;
            double incomeTax;
            double socialContribution;
            double netSalary;
            do
            {
                _writer.Write(_netCSpeaker.AskForGrossSalary());
                salaryInput = _reader.ReadLine();
                if (!_validator.ValidateSalaryInput(salaryInput))
                {
                    _writer.WriteLine(_netCSpeaker.AskForGrossSalaryAfterBadInput());
                    continue;
                }
                salary = double.Parse(salaryInput);
                netSalary = _netSalaryService.GetNetSalary(salary, _incomeTaxPercent, _socialContributionPercent);
                _writer.WriteLine(_netCSpeaker.ShowNetSalary(netSalary));
                _writer.WriteLine(_netCSpeaker.AskToShowIncomeTaxAndSocialContribution());
                answer = _reader.ReadLine().ToLower();
                if (!answer.Equals("no"))
                {
                    incomeTax = _incomeTaxService.GetIncomeTaxAmount(salary, _incomeTaxPercent);
                    socialContribution = _socialContributionService.GetSocialContributionAmount(salary, _socialContributionPercent);

                    _writer.WriteLine(_netCSpeaker.ShowIncomeTax(incomeTax));
                    _writer.WriteLine(_netCSpeaker.ShowSocialContribution(socialContribution));
                }
                _writer.WriteLine(_netCSpeaker.AskIfAnotherGrossSalaryShouldBeRead());
                answer = _reader.ReadLine().ToLower();
            } while (!answer.Equals("no"));
            _writer.WriteLine(_netCSpeaker.SayGoodBye());
        }
    }
}