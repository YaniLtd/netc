﻿using NetC.CLI.Contracts;

namespace NetC.CLI
{
    public class NetCSpeaker : INetCSpeaker
    {
        const string logo = @"$$\      $$\           $$\                                                     $$\                     $$\   $$\            $$\      $$$$$$\  
$$ | $\  $$ |          $$ |                                                    $$ |                    $$$\  $$ |           $$ |    $$  __$$\ 
$$ |$$$\ $$ | $$$$$$\  $$ | $$$$$$$\  $$$$$$\  $$$$$$\$$$$\   $$$$$$\        $$$$$$\    $$$$$$\        $$$$\ $$ | $$$$$$\ $$$$$$\   $$ /  \__|
$$ $$ $$\$$ |$$  __$$\ $$ |$$  _____|$$  __$$\ $$  _$$  _$$\ $$  __$$\       \_$$  _|  $$  __$$\       $$ $$\$$ |$$  __$$\\_$$  _|  $$ |      
$$$$  _$$$$ |$$$$$$$$ |$$ |$$ /      $$ /  $$ |$$ / $$ / $$ |$$$$$$$$ |        $$ |    $$ /  $$ |      $$ \$$$$ |$$$$$$$$ | $$ |    $$ |      
$$$  / \$$$ |$$   ____|$$ |$$ |      $$ |  $$ |$$ | $$ | $$ |$$   ____|        $$ |$$\ $$ |  $$ |      $$ |\$$$ |$$   ____| $$ |$$\ $$ |  $$\ 
$$  /   \$$ |\$$$$$$$\ $$ |\$$$$$$$\ \$$$$$$  |$$ | $$ | $$ |\$$$$$$$\         \$$$$  |\$$$$$$  |      $$ | \$$ |\$$$$$$$\  \$$$$  |\$$$$$$  |
\__/     \__| \_______|\__| \_______| \______/ \__| \__| \__| \_______|         \____/  \______/       \__|  \__| \_______|  \____/  \______/";

        public string AskForGrossSalary()
        {
            return "Enter the gross value of a salary in IDR and see the net amount (use dot for floating point): ";
        }
        public string AskForGrossSalaryAfterBadInput()
        {
            return "Please enter a non negative number only (use dot for floating point): ";
        }
        public string AskIfAnotherGrossSalaryShouldBeRead()
        {
            return "Do you want to see the net value of another salary? (yes/no)";
        }
        public string AskToShowIncomeTaxAndSocialContribution()
        {
            return "Do you want to see the income tax and the social contribution of the entered gross salary? (yes/no)";
        }
        public string DisplayLogo()
        {
            return logo;
        }
        public string SayGoodBye()
        {
            return "Good bye :)";
        }
        public string ShowIncomeTax(double incomeTax)
        {
            return $"The income tax paid is {incomeTax} IDR";
        }
        public string ShowNetSalary(double netSalary)
        {
            return $"Net salary is {netSalary} IDR";
        }
        public string ShowSocialContribution(double socialContribution)
        {
            return $"The social contribution is {socialContribution} IDR";
        }
    }
}